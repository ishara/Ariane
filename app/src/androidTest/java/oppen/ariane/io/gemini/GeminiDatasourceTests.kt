package oppen.ariane.io.gemini

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import oppen.ariane.Ariane
import oppen.toURI
import com.google.common.truth.Truth.assertThat
import oppen.ariane.io.GemState
import oppen.ariane.io.database.ArianeDatabase
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class GeminiDatasourceTests {

    private lateinit var gemini: Datasource
    private val capsules = listOf(
        "gemini://gemini.circumlunar.space",
        "gemini://rawtext.club",
        "gemini://drewdevault.com",
        "gemini://talon.computer",
        "gemini://tilde.team",
        "gemini://tilde.pink",
        "gemini://gemini.conman.org",
        "gemini://idiomdrottning.org"
    )

    private var capsuleIndex = 0

    @Before
    fun setup(){
        val capsule = capsules.random()
        println("Using $capsule for Gemini tests")
        capsuleIndex = capsules.indexOf(capsule)
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        val db = ArianeDatabase(appContext)
        gemini = Datasource.factory(InstrumentationRegistry.getInstrumentation().targetContext, db.history())
    }

    //todo- rebuild tests
}