package oppen.ariane.io.database

/**
 * Copyright © 2020 Öppenlab oppenlab.net
 */

import androidx.room.Database
import androidx.room.RoomDatabase
import oppen.ariane.io.database.bookmarks.BookmarkEntity
import oppen.ariane.io.database.bookmarks.BookmarksDao
import oppen.ariane.io.database.history.HistoryDao
import oppen.ariane.io.database.history.HistoryEntity

@Database(entities = [BookmarkEntity::class, HistoryEntity::class], version = 3)
abstract class ArianeAbstractDatabase: RoomDatabase() {
    abstract fun bookmarks(): BookmarksDao
    abstract fun history(): HistoryDao
}